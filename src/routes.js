'use strict';

const moment = require('moment');

// -------------------------------

module.exports = class Routes {
    constructor({ ws }) {
        this.ws = ws;
        this.init();
    }
    init() {
        this.ws.on('message', data => {
            const d = JSON.parse(data);
            switch (d.type) {
                case 'login':
                    if (d.login !== ''
                        && d.password !== ''
                        && d.gameDomain !== ''
                        && d.login !== undefined
                        && d.password !== undefined
                        && d.gameDomain !== undefined) {
                        this.sendData({
                            type: 'login',
                            status: 'ok',
                            rejReason: '',
                            userId: 92734,
                            userRank: 'Capitan',
                        });
                    } else {
                        this.sendData({
                            type: 'login',
                            status: 'cancel',
                            rejReason: 'has empty fields',
                        });
                    }
                    break;
                case 'game-list':
                    if (d.params !== undefined
                        && d.params.quantityOfGames !== undefined
                        && +d.params.quantityOfGames > 0
                        && !isNaN(d.params.quantityOfGames)) {
                        this.sendData({
                            type: 'game-list',
                            listGames: dummyList(d.params.quantityOfGames),
                        });
                    } else {
                        this.sendData({
                            type: 'game-list',
                            status: 'check params.quantityOfGames'
                        });
                    }
                    break;
                case 'game-details':
                    if (d.gameId !== undefined
                        && d.userId !== undefined
                        && d.gameId !== ''
                        && d.userId !== ''
                        && d.gameId !== 0
                        && d.userId !== 0) {
                        this.sendData({
                            type: 'game-details',
                            data: {
                                status: 'reject',
                                reason: 'not submitted by the team',
                            }
                        });
                    } else {
                        this.sendData({
                            type: 'game-details',
                            status: 'check gameId and userId'
                        });
                    }
                    break;

                default:
                    break;
            }
        });
    }
    sendData(data = {}) {
        this.ws.send(JSON.stringify(data));
    }
}

const dummyList = (q) => {
    const tmp = [];
    for (let i = 0; i < q; i++) {
        tmp.push({
            name: 'qMir', // game name
            gameId: Math.round(Math.random() * 100000),
            organizators: ['spezza'], // array of organizators
            startTime: moment().format('DD/MMM/YYYY HH:mm'), // time of start 
            status: 'inactive', // statuses or active or await
        })
    }
    return tmp;
}

