'use strict';

const WebSocket = require('ws');
const { v4: uuidv4 } = require('uuid');

const Routes = require('./routes');

// -------------------------

module.exports = class App {
    constructor() {
        this.socket = new WebSocket.Server({ host: '127.0.0.1', port: 8080 });
        this.wsClients = new Map();
        // -------------------
        this.init();
    }
    init() {
        function noop() {
            // console.log('noop');
        }

        function heartbeat() {
            this.isAlive = true;
        }

        this.socket.on('connection', ws => {

            const uid = uuidv4();
            ws.uid = uid;

            ws.isAlive = true;
            ws.on('pong', heartbeat);
            
            this.wsClients.set(uid, {
                ws: new Routes({ ws }),
                close: ws.on('close', () => {
                    this.wsClients.delete(uid);
                }),
            });
        });

        const interval = setInterval(() => {
            this.socket.clients.forEach(function each(ws) {
                if (ws.isAlive === false) return ws.terminate();
                ws.isAlive = false;
                ws.ping(noop);
            });
        }, 1000 * 60 * 5);
    }
}