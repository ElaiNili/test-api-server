'use strict';

const WebSocket = require('ws');

// -------------------------

const ws = new WebSocket('ws://127.0.0.1:8080');

ws.on('open', function open() {
    console.log('Connected to server');
    // ----------- login ------ 

    // ws.send(JSON.stringify({
    //     type: 'login',
    //     login: 'spezza', // user login
    //     password: '123', // user password
    //     gameDomain: 'quest.ua', // example: quest.ua
    // }));

    // ----------- game list ------ 

    // ws.send(JSON.stringify({
    //     type: 'game-list',
    //     params: {
    //         quantityOfGames: 10,
    //     }
    // }));

    // ----------- game details ------

    // ws.send(JSON.stringify({
    //     type: 'game-details',
    //     gameId: 1324,
    //     userId: 92734,
    // }));

});

ws.on('message', function incoming(data) {
    console.log(JSON.parse(data));
});
