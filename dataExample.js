'use strict';

// in to server
// out yo client

// ------------- auth ---------------
const inLogin = {
    type: 'login',
    login: '', // user login
    password: '', // user password
    gameDomain: '', // example: quest.ua
}
const outLogin = {
    type: 'login',
    status: 'ok', // or reject
    rejReason: '', // if reject send reason
    userId: 92734, // use id
    userRank: 'Capitan', // user rank
}
// ------------- games list ---------------

const inGameList = {
    type: 'game-list',
    params: {
        quantityOfGames: 50,
    }
}
const outGameList = {
    type: 'game-list',
    listGames: [
        {
            name: 'qMir', // game name
            gameId: 1324,
            organizators: ['spezza'], // array of organizators
            startTime: '2021-04-23 14:00:00', // time of start 
            status: [''], // statuses or 1 active or 0 await
        }
    ]
}

// ------------- game details ---------------

const inGameDetails = {
    type: 'game-details',
    gameId: 1324,
    userId: 92734,
}

// if game inactive or active
// check user status
// send if not access
const outGameDetails = {
    type: 'game-details',
    data: {
        status: 'reject',
        reason: 'not submitted by the team',
        // reason: 'not submitted by a player',
        // reason: 'the team not accepted in game',
        // reason: 'a player not accepted in game',
        // reason: 'a number of participants is exceedsed',
    }
}

